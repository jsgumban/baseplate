<?php

namespace App\Providers;

use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;

class RequestMacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // A helper to easily configure and get the per page or limit request
        Request::macro('perPage', function ($perPage = 10) {
            return (int) $this->input('per_page', $this->input('limit', $perPage));
        });
    }
}
