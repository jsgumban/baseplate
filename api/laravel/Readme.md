# API Baseplate

Boilerplate for backend API

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Laravel Version
Laravel 5.8

## Prerequisites

php extensions

* BCMath PHP Extension
* Ctype PHP Extension
* JSON PHP Extension
* Mbstring PHP Extension
* OpenSSL PHP Extension
* PDO PHP Extension
* SQLite 3 PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension
* Exif PHP Extension

### Installing

A step by step series of examples that tell you how to get a development env running

* Make sure you have composer installed on your local machine `composer install`
* Create .env file from .env.example `cp .env.example .env`
* Update .env based on your local configuration
* Generate application key `php artisan key:generate`
* Generate jwt key `php artisan jwt:secret`
* You can serve the application on LAMP/XAMPP/LEMP stack or use its builtin server `php artisan serve`
* To seed sample data run `php artisan db:seed --class=DevelopmentSeeder`

## Commands
* `php artisan app:acl:sync` - This will sync user roles and permissions that defined in `app/Enums/Role.php` and `app/Enums/Permission.php` to the database. code can be found in `app/Console/Commands/AclSync.php`

## Testing

For testing, Configuration can be found in `.env.testing`

* We need to add database and test user that will going to use in running testing
```mysql
CREATE DATABASE mysql_test_database;
CREATE USER 'mysql_test_user'@'localhost'
  IDENTIFIED 
  	WITH mysql_native_password 
  	BY 'mysql_test_password';
GRANT ALL
  ON mysql_test_database.*
  TO 'mysql_test_user'@'localhost'
  WITH GRANT OPTION;
```
* To run the test, run this on your terminal `vendor/bin/phpunit`
* To create test, you need to mimic the file directory of the code you want to test. Please see `test/` folder for example

## Coding Standards

We're using PSR2 for coding standards, configuration can be found in `phpcs.xml`
* To check if your code follow the standard, run this on your terminal `vendor/bin/phpcs`

## Gitlab CI

We're using gitlab-ci for continuous integration and deployments, configuration can be found in `.gitlab-ci.yml`

Gitlab-ci will automatically run when you push to master or develop branch, this will run `phpunit` to check if your commit passes every test and also run `phpcs` to check for coding standards. This can be viewed on your project [pipelines](https://gitlab.com/appetiser/baseplate-api/pipelines). Also push to deploy can be configured here.

## Libraries

* [https://github.com/BenSampo/laravel-enum](https://github.com/BenSampo/laravel-enum)
* [https://github.com/spatie/laravel-cors](https://github.com/spatie/laravel-cors)
* [https://github.com/spatie/laravel-medialibrary](https://github.com/spatie/laravel-medialibrary)
* [https://github.com/spatie/laravel-permission](https://github.com/spatie/laravel-permission)
* [https://github.com/spatie/laravel-query-builder](https://github.com/spatie/laravel-query-builder)
* [https://github.com/spatie/laravel-queueable-action](https://github.com/spatie/laravel-queueable-action)
* [https://github.com/tymondesigns/jwt-auth](https://github.com/tymondesigns/jwt-auth)

## PR
If you found any bugs, issues, typos or you want to add some changes, [merge request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html) is very much appreciated. Thanks!
