<?php

namespace Tests\Feature\Controllers\V1\Auth\AuthController;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CheckEmailTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function registeredUserCanCheckAccountExistViaEmail()
    {
        $user = create(User::class);

        $res = $this->json('POST', route('auth.checkEmail'), [
            'email' => $user->email,
        ]);

        $res->assertOk()
            ->assertJsonStructure([
                'data' => ['email'],
            ])
            ->assertJson([
                'data' => ['email' => $user->email]
            ]);

        $this->assertGuest();
    }

    /** @test */
    public function unregisteredEmailShouldReturnNotFound()
    {
        $this->json('POST', route('auth.checkEmail'), [
            'email' => 'unregistered@email.com',
        ])->assertStatus(404);
    }

    /** @test */
    public function itShouldValidateEmail()
    {
        // email is required
        $this->json('POST', route('auth.checkEmail'))
            ->assertStatus(422)
            ->assertJsonStructure([
                'errors' => ['email'],
            ]);

        // invalid email format
        $this->json('POST', route('auth.checkEmail'), [
            'email' => 'not_an_email'
        ])->assertStatus(422)
            ->assertJsonStructure([
                'errors' => ['email'],
            ]);

        // should not accept a phone_number
        $this->json('POST', route('auth.checkEmail'), [
            'email' => '+639453200575'
        ])->assertStatus(422)
            ->assertJsonStructure([
                'errors' => ['email'],
            ]);
    }
}
