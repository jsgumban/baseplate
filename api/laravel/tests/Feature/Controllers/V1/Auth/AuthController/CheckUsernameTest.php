<?php

namespace Tests\Feature\Controllers\V1\Auth\AuthController;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CheckUsernameTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function registeredUserCanCheckAccountExistViaEmail()
    {
        $user = create(User::class);

        $res = $this->json('POST', route('auth.checkUsername'), [
            'username' => $user->email,
        ]);

        $res->assertOk()
            ->assertJsonStructure([
                'data' => ['username'],
            ])
            ->assertJson([
                'data' => ['username' => $user->email]
            ]);

        $this->assertGuest();
    }

    /** @test */
    public function registeredUserCanCheckAccountExistViaPhoneNumber()
    {
        $user = create(User::class, [
            "phone_number" => '+63945' . rand(1000000, 9999999)
        ]);

        $this->json('POST', route('auth.checkUsername'), [
            'username' => $user->phone_number,
        ])
            ->assertOk()
            ->assertJsonStructure([
                'data' => ['username'],
            ])
            ->assertJson([
                'data' => ['username' => $user->phone_number]
            ]);

        $this->assertGuest();
    }

    /** @test */
    public function unregisteredUsernameShouldReturnNotFound()
    {
        // test unregistered number
        $this->json('POST', route('auth.checkUsername'), [
            'username' => 'invalid@email.com',
        ])->assertStatus(404);

        // test unregistered number
        $this->json('POST', route('auth.checkUsername'), [
            'username' => '+639451111111',
        ])->assertStatus(404);
    }

    /** @test */
    public function usernameIsRequired()
    {
        $this->json('POST', route('auth.checkUsername'))
            ->assertStatus(422)
            ->assertJsonStructure([
                'errors' => ['username'],
            ]);
    }
}
