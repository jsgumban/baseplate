<?php

namespace Tests\Feature\Controllers\V1\Auth\AuthController;

use Tests\TestCase;
use App\Models\User;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LogoutTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function authenticatedUserCanLogout()
    {
        $user = create(User::class);
        $token = JWTAuth::fromUser($user);

        $this->json('POST', route('auth.logout'), [], ['Authorization' => "Bearer $token"])->assertOk();

        $this->assertGuest();
    }

    /** @test */
    public function unauthenticatedUserCannotLogout()
    {
        $this->json('POST', route('auth.logout'))->assertStatus(401);
    }
}
