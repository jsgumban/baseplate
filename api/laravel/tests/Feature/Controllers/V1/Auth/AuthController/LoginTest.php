<?php

namespace Tests\Feature\Controllers\V1\Auth\AuthController;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function registeredUserCanLoginViaEmail()
    {
        $user = create(User::class);

        $res = $this->json('POST', route('auth.login'), [
            'username' => $user->email,
            'password' => 'password',
        ]);

        $res->assertOk()
            ->assertJsonStructure([
                'data' => ['access_token', 'token_type', 'expires_in'],
            ]);

        $this->assertAuthenticated();
        $this->assertAuthenticatedAs($user);
    }

    /** @test */
    public function registeredUserCanLoginViaPhoneNumber()
    {
        $user = create(User::class, [
            'phone_number' => '+639452303949',
        ]);

        $res = $this->json('POST', route('auth.login'), [
            'username' => $user->phone_number,
            'password' => 'password',
        ])
            ->assertOk()
            ->assertJsonStructure([
                'data' => ['access_token', 'token_type', 'expires_in'],
            ]);

        $this->assertAuthenticated();
        $this->assertAuthenticatedAs($user);
    }

    /** @test */
    public function unregisteredUsersCannotLogIn()
    {
        $this->json('POST', route('auth.login'), [
            'username' => 'some.random@email.com',
            'password' => 'password'
        ])->assertStatus(401);
    }

    /** @test */
    public function wrongUserPasswordCannotLogIn()
    {
        $user = create(User::class, ['email_verified_at' => null]);

        $this->json('POST', route('auth.login'), [
            'username' => $user->email,
            'password' => 'th!$_!$_n()t_ah_p@ssw0rd',
        ])->assertStatus(401);
    }
}
