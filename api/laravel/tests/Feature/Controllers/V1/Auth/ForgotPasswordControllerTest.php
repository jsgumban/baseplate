<?php

namespace Tests\Feature\Controllers\V1\Auth;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Mail;
use App\Mail\PasswordReset;
use App\Models\PasswordReset as PasswordResetModel;

class ForgotPasswordControllerTest extends TestCase
{

    use RefreshDatabase;

    /** @test */
    public function userCanRequestResetPasswordToken()
    {
        $user = create(User::class);

        Mail::fake();
        Mail::assertNothingSent();

        $this->json('POST', route('forgotPassword'), [
            'email' => $user->email,
        ])
            ->assertStatus(201)
            ->assertJsonStructure([
                'data' => ['email', 'expires_at', 'created_at'],
            ])
            ->assertJson([
                'data' => ['email' => $user->email],
            ]);

        $this->assertDatabaseHas('password_resets', [
            'email' => $user->email,
        ]);

        $passwordReset = PasswordResetModel::find($user->email);

        Mail::assertSent(PasswordReset::class, function ($mail) use ($user, $passwordReset) {
            return $mail->hasTo($user->email) &&
                $mail->hasTo($passwordReset->email) &&
                $mail->passwordReset->token = $passwordReset->token;
        });
    }

    /** @test */
    public function emailMustBeValidated()
    {
        // Email field is required
        $this->json('POST', route('forgotPassword'), [
            'email' => '',
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['email'],
            ]);

        // Email must be valid
        $this->json('POST', route('forgotPassword'), [
            'email' => 'not_a_valid_email',
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['email'],
            ]);

        // Email must exists
        $this->json('POST', route('forgotPassword'), [
            'email' => 'notexisting@email.xxx',
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['email'],
            ]);
    }
}
