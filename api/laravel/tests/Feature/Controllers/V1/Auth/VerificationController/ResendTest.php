<?php

namespace Tests\Feature\Controllers\V1\Auth\VerificationController;

use Tests\TestCase;
use App\Models\User;
use App\Notifications\VerifyEmail;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Notification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Notifications\VerifyPhoneNumber;

class ResendTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function unverifiedUserCanResendEmailVerificationToken()
    {
        Notification::fake();
        Notification::assertNothingSent();

        $user = create(User::class, [
            'email_verified_at' => null,
        ]);

        $token = JWTAuth::fromUser($user);

        $this->json(
            'POST',
            route('verification.resend'),
            ['via' => 'email'],
            ['Authorization' => "Bearer $token"]
        )->assertOk();

        Notification::assertSentTo(
            $user,
            VerifyEmail::class,
            function ($notification) use ($user) {
                $notifier = $notification->toMail($user);
                return $notifier->user === $user;
            }
        );
    }

    /** @test */
    public function unverifiedUserCanResendPhoneNumberVerificationToken()
    {
        Notification::fake();
        Notification::assertNothingSent();

        $user = create(User::class, [
            'phone_number' => '+639453222049',
            'phone_number_verified_at' => null,
        ]);

        $token = JWTAuth::fromUser($user);

        $this->json(
            'POST',
            route('verification.resend'),
            ['via' => 'phone_number'],
            ['Authorization' => "Bearer $token"]
        )->assertOk();

        Notification::assertSentTo($user, VerifyPhoneNumber::class);
    }
}
