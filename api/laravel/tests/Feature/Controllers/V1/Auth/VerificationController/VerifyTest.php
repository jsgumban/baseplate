<?php

namespace Tests\Feature\Controllers\V1\Auth\VerificationController;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class VerifyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function unverifiedUserCanVerifyEmail()
    {
        $user = create(User::class, [
            'email_verified_at' => null,
        ]);

        $token = JWTAuth::fromUser($user);

        $this->json(
            'POST',
            route('verification.verify'),
            ['via' => 'email', 'token' => $user->email_verification_code],
            ['Authorization' => "Bearer $token"]
        )->assertOk();


        tap($user->fresh(), function ($user) {
            $this->assertNotNull($user->email_verified_at);
        });
    }

    /** @test */
    public function unverifiedUserCanVerifyPhoneNumber()
    {
        $user = create(User::class, [
            'phone_number' => '639453348354',
            'phone_number_verified_at' => null,
        ]);

        $token = JWTAuth::fromUser($user);

        $this->json(
            'POST',
            route('verification.verify'),
            ['via' => 'phone_number', 'token' => $user->phone_number_verification_code],
            ['Authorization' => "Bearer $token"]
        )->assertOk();


        tap($user->fresh(), function ($user) {
            $this->assertNotNull($user->phone_number_verified_at);
        });
    }

    /** @test */
    public function tokenShouldBeValidated()
    {
        $user = create(User::class, [
            'email_verified_at' => null,
        ]);

        $token = JWTAuth::fromUser($user);

        // token is required
        $this->json('POST', route('verification.verify'), [
            'token' => '',
            'via' => 'email'
        ], ['Authorization' => "Bearer $token"])
            ->assertStatus(422)
            ->assertJsonStructure(['message', 'errors' => ['token']]);

        // token must be equal to user email_verification_token
        $this->json('POST', route('verification.verify'), [
            'token' => Str::random(5), // some random token
            'via' => 'email'
        ], ['Authorization' => "Bearer $token"])
            ->assertStatus(422)
            ->assertJsonStructure(['message', 'errors' => ['token']]);
    }

    /** @test */
    public function viaShouldBeValidated()
    {
        $user = create(User::class, [
            'email_verified_at' => null,
        ]);

        $token = JWTAuth::fromUser($user);

        // via is required
        $this->json('POST', route('verification.verify'), [
            'token' => $user->email_verification_code,
            'via' => ''
        ], ['Authorization' => "Bearer $token"])
            ->assertStatus(422)
            ->assertJsonStructure(['message', 'errors' => ['via']]);

        $this->json('POST', route('verification.verify'), [
            'token' => Str::random(5),
            'via' => 'facebook' // via not exist
        ], ['Authorization' => "Bearer $token"])
            ->assertStatus(422)
            ->assertJsonStructure(['message', 'errors' => ['via']]);
    }
}
