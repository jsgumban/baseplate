<?php

namespace Tests\Feature\Controllers\V1\UserAvatarController;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StoreTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function itShouldBeAbleToUploadUserAvatar()
    {
        $user = create(User::class);

        $res = $this->actingAs($user)
            ->json('POST', route('user.avatar.store', ['id' => $user->id]), [
                'avatar' => UploadedFile::fake()->image('avatar.png')
            ]);

        $res->assertStatus(201)
            ->assertJsonStructure([
                'data' => ['id', 'name', 'file_name', 'collection_name', 'url', 'thumb_url']
            ]);

        $this->assertDatabaseHas('media', [
            'model_type' => User::class,
            'model_id' => $user->id,
            'collection_name' => 'avatar'
        ]);
    }

    /** @test */
    public function itShouldRedirectToUserAvatarImage()
    {
        $user = create(User::class);

        $this->actingAs($user)
            ->json('POST', route('user.avatar.store', ['id' => $user->id]), [
                'avatar' => UploadedFile::fake()->image('avatar.png')
            ]);

        $res = $this->actingAs($user)
            ->getJson(route('user.avatar.show', ['id' => $user->id]));

        $res->assertStatus(302);
    }

    /** @test */
    public function itShouldReturnAvatarResource()
    {
        $user = create(User::class);

        $this->actingAs($user)
            ->json('POST', route('user.avatar.store', ['id' => $user->id]), [
                'avatar' => UploadedFile::fake()->image('avatar.png')
            ]);

        $res = $this->actingAs($user)
            ->getJson(route('user.avatar.show', ['id' => $user->id, 'redirect' => false]));

        $res->assertOk()->assertJsonStructure([
            'data' => ['id', 'name', 'file_name', 'collection_name', 'url', 'thumb_url']
        ]);
    }
}
