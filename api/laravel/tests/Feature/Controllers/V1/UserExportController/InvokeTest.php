<?php

namespace Tests\Feature\Controllers\V1\UserExportController;

use App\Exports\UserExport;
use Tests\TestCase;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\User;

class InvokeTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /** @test */
    public function authenticatedUserShouldBeAbleToExportUserListToExcel()
    {
        Excel::fake();
        $users = create(User::class, [], 10);

        $this->actingAs($users[0])->get(route('users.export'))->assertOk();

        Excel::assertDownloaded('users.xlsx', function (UserExport $export) use ($users) {
            return $export->collection()->count() === $users->count();
        });
    }

    /** @test */
    public function unauthenticatedUserShouldNotBeAbleToExportUserListToExcel()
    {
        $users = create(User::class, [], 10);

        $this->getJson(route('users.export'))->assertStatus(401);
    }
}
