<?php

namespace Tests\Feature\Controllers\V1\UserController;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ShowTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function authenticatedUserCanViewUserDetails()
    {
        $user = create(User::class);
        $user2 = create(User::class);

        $this->actingAs($user)
            ->getJson(route('users.show', ['user' => $user->id]))
            ->assertOk()
            ->assertJsonStructure([
                'data' => ['id', 'first_name', 'last_name', 'full_name', 'email', 'phone_number']
            ]);

        $this->actingAs($user)
            ->getJson(route('users.show', ['user' => $user2->id]))
            ->assertOk();
    }

    /** @test */
    public function unauthenticatedUserCannotViewUserDetails()
    {
        $user = create(User::class);

        $this->getJson(route('users.show', ['user' => $user->id]))
            ->assertStatus(401);
    }
}
