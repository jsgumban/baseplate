import Vue from 'vue'
import App from './App.vue'
import router from './routes'
import store from './store/index'
import './assets/scss/main.scss'
import './registerServiceWorker'
import './api'
import './env'
import './models'
import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue)

Vue.config.productionTip = false
store.dispatch('auth/init')

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
