import User from '@/models/User'
import Api from '@/api'
import map from 'lodash/map'

export default {
  namespaced: true,
  state: {
    list: [],
    listMeta: {},
    userDetails: null
  },

  mutations: {
    setUserList (state, users) {
      state.list = map(users, (user) => {
        if (user instanceof User) {
          return user
        } else {
          return new User(user)
        }
      })
    },

    setUserListMeta (state, meta) {
      state.listMeta = meta
    },

    setUserDetails (state, user) {
      if (user instanceof User) {
        state.userDetails = user
      } else {
        state.userDetails = new User(user)
      }
    },

    clearUserDetails (state) {
      state.userDetails = null
    }

  },

  actions: {
    async getUsers ({ commit }, params) {
      const query = User.page(params.page || 1)

      if (params.search) {
        query.where('search', params.search)
      }

      const res = await query.get()

      commit('setUserList', res.data)
      commit('setUserListMeta', res.meta)
    },

    async getUserDetails ({ commit }, id) {
      const { data } = await Api.get(`users/${id}`)
      commit('setUserDetails', data.data)
    },

    async updateUser ({ commit }, formData) {
      await Api.put(`users/${formData.id}`, formData)
    }
  }
}
