import assignIn from 'lodash/assignIn'
import cloneDeep from 'lodash/cloneDeep'
import each from 'lodash/each'

class Form {
  constructor (values) {
    this.$errorMessage = ''
    this.$errors = {}
    this.$busy = false
    // this.$focus = cloneDeep(values)
    this.$originalData = cloneDeep(values)
    assignIn(this, values)
  }

  /**
   * Returns the values based on original fields
   *
   * @returns {Object}
   */
  $data () {
    let data = cloneDeep(this.$originalData)
    each(this.$originalData, (_, key) => {
      data[key] = this[key]
    })

    return data
  }

  /**
   * Reset the form to its original phase
   * Set the original data values
   * Clear errors
   */
  $reset () {
    assignIn(this, this.$originalData)
    this.$clearErrors()
  }

  /**
   * Set the form errors
   * @param {Object, Array} errors
   */
  $setErrors (errors) {
    this.$errors = errors
  }

  /**
   * Clear all form errors
   */
  $clearErrors () {
    this.$errors = {}
  }

  /**
   * Clear the errors in a specific field
   *
   * @param {String} field
   */
  $clearError (field) {
    delete this.$errors[field]
  }

  /**
   * Check if the field has error
   *
   * @param {String} field
   * @returns {Boolean}
   */
  $hasError (field) {
    return !!this.$errors[field]
  }

  /**
   * Get the error description of a specific field
   *
   * @param {String} field
   * @returns {String}
   */
  $getError (field) {
    return this.$errors[field]
  }

  /**
   * Set the form error message
   *
   * @param {String} message
   */
  $setErrorMessage (message) {
    this.$errorMessage = message
  }

  /**
   * Use when applying a custom label style when input is focused
   * Remove or customize depending on the application specs
   *
   * For other input field style and effects, refer to:
   *    Component: @/views/demo/InputFieldsDemo.vue
   *    Route: /demo/input-fields
   *
   * @param {String} field
   * @param {Boolean} isFocus
   */
  // $setFocus (field, isFocus) {
  //   this.$focus[field] = isFocus
  // }
  // $getLabelClass (field) {
  //   return this.$focus[field] ? 'label-focus' : 'label-default'
  // }

  /**
   * Handle API error response data
   * Customize depending on the application specs
   *
   * @param {Object, Array} error
   */
  $handleError (error) {
    this.$busy = false
    if (!error.response) { // frontend error
      console.log(error)
    } else if (error.response.status === 422) { // for invalid form input error
      this.$errors = error.response.data.errors
      this.$errorMessage = ''
    } else { // for other API errors
      this.$errorMessage = error.response.data.message
    }
  }
}

export default Form
