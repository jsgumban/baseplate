import Vue from 'vue'
import Router from 'vue-router'
import auth from './auth'
import user from './user'
import demo from './demo'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    auth,
    user,
    demo,
    {
      path: '/',
      redirect: { name: 'auth.login-page' }
    }
  ]
})
