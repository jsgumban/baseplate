export default {
  path: '/auth',
  name: 'auth',
  redirect: { name: 'auth.login-page' },
  component: () => import(/* webpackChunkName: "auth" */ '@/views/auth'),
  children: [
    {
      path: '/check-email',
      name: 'auth.check-email-page',
      component: () => import(/* webpackChunkName: "check-email" */ '@/views/auth/CheckEmailPage.vue')
    },
    {
      path: '/login',
      name: 'auth.login-page',
      component: () => import(/* webpackChunkName: "login" */ '@/views/auth/LoginPage.vue')
    },
    {
      path: '/register',
      name: 'auth.register-page',
      component: () => import(/* webpackChunkName: "register" */ '@/views/auth/RegisterPage.vue')
    },
    {
      path: '/forgot-password',
      name: 'auth.forgot-password-page',
      component: () => import(/* webpackChunkName: "forgot-password" */ '@/views/auth/ForgotPasswordPage.vue')
    },
    {
      path: '/reset-password',
      name: 'auth.reset-password-page',
      component: () => import(/* webpackChunkName: "reset-password" */ '@/views/auth/ResetPasswordPage.vue')
    }
  ]
}
