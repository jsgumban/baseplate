export default {
  path: '/user',
  name: 'user',
  redirect: { name: 'user.profile-page' },
  component: () => import(/* webpackChunkName: "user" */ '@/views/user'),
  children: [
    {
      path: '/profile',
      name: 'user.profile-page',
      component: () => import(/* webpackChunkName: "profile-page" */ '@/views/user/ProfilePage.vue')
    }
  ]
}
