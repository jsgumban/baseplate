export default {
  path: '/demo',
  name: 'demo',
  component: () => import(/* webpackChunkName: "demo" */ '@/views/demo'),
  children: [
    {
      path: '/demo/input-fields',
      name: 'demo.input-fields-demo',
      component: () => import(/* webpackChunkName: "input-fields-demo" */ '@/views/demo/InputFieldsDemo.vue')
    }
  ]
}
